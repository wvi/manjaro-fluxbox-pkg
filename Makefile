SHELL=/bin/bash
BRANCH=stable
ARCH=x86_64
REPO=pkgs

PKGS=$(patsubst pkgbuilds/%/,%,$(wildcard pkgbuilds/*/))

.PHONY: build upload clean

all: build

build:
	@cd pkgbuilds && for p in ${PKGS}; do echo "Building package $${p}"; buildpkg -p $${p}; done
	@mkdir -p ${REPO}
	@cp -r /var/cache/manjaro-tools/pkg/${BRANCH}/${ARCH}/* ${REPO}/
	@repo-add ${REPO}/${REPO}.db.tar.gz ${REPO}/*.pkg.tar.*
	@rm -f ${REPO}/${REPO}.db && cp ${REPO}/${REPO}.db.tar.gz ${REPO}/${REPO}.db
	@rm -f ${REPO}/${REPO}.files && cp ${REPO}/${REPO}.files.tar.gz ${REPO}/${REPO}.files
	@rm -rf ${REPO}/*.old

upload: clean
	@git add -f ${REPO}
	git status
	@git commit -m "update pkg repository"
	echo "Pushing to origin master"
	@git push origin master

clean:
	cd pkgbuilds && for p in ${PKGS}; do echo "Cleaning package $${p}"; find $${p}/* -type d | xargs rm -rf ; done
